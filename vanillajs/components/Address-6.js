Vue.component('mc-address', {
  template: `<div>
    <strong>Address {{label}}</strong><br/>
    Road : {{ address.road }}<br/>
    Zip Code : {{ address.zipCode }}<br/>
    City : {{ address.city | uppercase }}<br/>
    <span v-if="address.complement">Complement: {{ address.complement }}<br/></span>
    <span v-if="address.country">Country: {{ address.country }}<br/></span>
    <span v-if="address.state">State: {{ address.state | uppercase }}<br/></span>
  </div>`,
  props: ['address', 'label']
});
