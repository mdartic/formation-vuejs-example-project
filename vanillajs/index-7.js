// import Address from './components/Address-7.js';
// import AddressForm from './components/AddressForm-7.js';

Vue.filter('uppercase', (value) => {
  if (!value) return '';
  return value.toUpperCase();
});

var app = new Vue({
  el: '#app',
  vuetify: new Vuetify(),
  data: {
    from: {
      road: '52 rue Jacques Babinet',
      zipCode: '31000',
      city: 'Toulouse',
      complement: 'Makina Corpus',
      country: 'Occitanie'
    }, to: {
      road: 'place du Capitole',
      zipCode: '31000',
      city: 'Toulouse',
      state: 'France'
    }
  },
})
